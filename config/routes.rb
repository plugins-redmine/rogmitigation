# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
get 'rogmitigation', :to => 'rogmitigations#index'
post 'post/rogmitigation/save', :to => 'rogmitigations#save'
post 'post/rogmitigation/delete', :to => 'rogmitigations#delete'