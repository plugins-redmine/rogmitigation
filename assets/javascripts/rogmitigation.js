$(document).ready(function() {
	if($("body.controller-issues #content .contextual .icon.icon-time-add").length>0){
		xissueid=$('#issue-form').attr('action');
		xissueid=xissueid.replace('/issues/','');
		$("body.controller-issues #content .contextual .icon.icon-time-add").before('<a class="icon icon-add fancypopup-rogmitigation dnone" href="/rogmitigation/?issueid='+xissueid+'">Riesgo</a>');
		var w = 912, h = 520;
		$(".fancypopup-rogmitigation").click(function(){
			window.open($(this).attr('href')+'#rogged-rogmitigation','new','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width='+w+',height='+h+', top=100, left=100');
			return false;
		});

		function change_tipoissue(){
			if($('#issue_tracker_id').length > 0){
				xissue_tracker=$('#issue_tracker_id :selected').text();
				if(xissue_tracker.toLowerCase().trim() == 'proyectos' || xissue_tracker.toLowerCase().trim() == 'proyecto'){
					$('.icon-add.fancypopup-rogmitigation').removeClass('dnone');
				}else{
					$('.icon-add.fancypopup-rogmitigation').addClass('dnone');
				}
			}
		}
		change_tipoissue();
	}
	if($("body.controller-rogmitigations").length>0){
		$('.rogged-more').click(function(){
			if( $(this).parent().find('.dtext').hasClass('vmore') ){
				$(this).html('Minimizar detalle');
				$(this).parent().find('.dtext').removeClass('vmore').addClass('vmoreshow');
			}else{
				$(this).html('Ver completo');
				$(this).parent().find('.dtext').addClass('vmore').removeClass('vmoreshow');;
			}
			return false;
		});
	}
});