class CreateRogmitigations < ActiveRecord::Migration
  def change
    create_table :rogmitigations do |t|
      t.text :riesgo
      t.text :mitigation
      t.integer :issue_id, :default => 0
      t.integer :author_id, :default => 0
      t.datetime :created_on
      t.datetime :updated_on
    end
  end
end
