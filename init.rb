require_dependency 'rogmitigations_hook_listener'

Redmine::Plugin.register :rogmitigation do
  name 'Redmine Issue mitigation plugin'
  author 'Oscar Torres'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://orotorres.com'
  author_url 'http://orotorres.com'
end
