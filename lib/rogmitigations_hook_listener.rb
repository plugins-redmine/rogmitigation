class RogmitigationsHookListener < Redmine::Hook::ViewListener
	def view_layouts_base_html_head(context)
		'<link rel="stylesheet" type="text/css" href="/plugin_assets/rogmitigation/stylesheets/rogmitigation.css?v=1.0" media="screen" /><script type="text/javascript" src="/plugin_assets/rogmitigation/javascripts/rogmitigation.js?v=1.0"></script>'
	end
end
