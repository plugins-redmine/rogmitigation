class RogmitigationsController < ApplicationController
  def index
  	@rogmitigations = Rogmitigations.where(:issue_id => params[:issueid])
  end
  def save
  	if params[:xobjid].to_i > 0
  		@rogmitigations = Rogmitigations.find(params[:xobjid])
  		@rogmitigations.riesgo = params[:riesgo]
  		@rogmitigations.mitigation = params[:mitigation]
  		@rogmitigations.updated_on = Time.now
  		@rogmitigations.save
  	else
	  	@rogmitigations = Rogmitigations.new
	  	@rogmitigations.riesgo = params[:riesgo]
	    @rogmitigations.mitigation = params[:mitigation]
	    @rogmitigations.issue_id = params[:roggedissueid]
	    @rogmitigations.author_id = session[:user_id]
	    @rogmitigations.created_on = Time.now
	    @rogmitigations.updated_on = Time.now
	    @rogmitigations.save
	end
	redirect_to :back
  end
  def delete
    @rogmitigations = Rogmitigations.find(params[:id])
    @rogmitigations.delete
    redirect_to :back
  end
end
